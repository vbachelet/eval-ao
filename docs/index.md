# Projet de recherche : Évaluation et valorisation des actifs immatériels ouverts

## 1 - Contexte 

L’évaluation et la valorisation des actifs ouverts est un sujet peu abordé jusqu’à présent ou faisant état de travaux de recherche disparates non-spécifiques alors que les modèles d’innovation ouverte se développent et que les méthodologies d’évaluation des actifs actuellement utilisées ne sont pas adaptées à leurs spécificités. En effet, ces méthodologies reposent sur l’exploitation d’un monopole au profit d’un titulaire de droits. La valeur entretient alors un lien étroit avec la notion d’exclusivité et de contrôle de diffusion de la technologie. A l’inverse, les ressources au sein des modèles ouverts sont partagées et ont vocation à être le plus largement possible diffusées et utilisées. En conséquence, la valeur des actifs présents ne repose pas sur les mêmes indicateurs – bien qu’il conviendra de s’en inspirer. 

## 2 - Présentation du projet

Ce projet consiste à élaborer une méthodologie d’évaluation des actifs ouverts c’est-à-dire des logiciels et des données ayant été placés sous une licence garantissant une liberté d’accès, de modification et de distribution. En d’autres termes, il s’agit de mesurer les actifs dans le cadre des projets Open Source et Open Data. 

Cette méthodologie a vocation à être utilisée par : 
    • les organisations qui choisissent d’ouvrir un projet et celles qui décident d’utiliser des ressources ouvertes tierces ; 
    • les organisations qui fonctionnent déjà dans une logique d’ouverture en ayant recours à l’Open Source et à l’Open Data, et celles qui n’ont pas encore franchi le pas. 

## 3 - Les enjeux 

Les enjeux sont multiples : 
    • confirmer scientifiquement la valeur des projets Open Source et Open Data ; 
    • rassurer les acteurs n’étant pas entrés dans cette démarche ; 
    • véhiculer une confiance accrue à l’égard de ces modèles ; 
    • valoriser au mieux ces projets ; 
    • ajuster ou définir des modèles économiques adaptés. 
A notre sens, cette méthodologie permettra à terme de stimuler la diffusion des modèles Open Source et Open Data. 

De plus, il y a actuellement une forte demande de la part tant des acteurs privés que publics au regard notamment du contexte législatif actuel (exemple : la Loi pour une République numérique du 6 octobre 2016 a élargi les obligations en matière d’Open Data à charge des administrations). 

