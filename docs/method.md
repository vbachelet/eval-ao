# La méthode


Inno³ et différents chercheurs (principalement Nicolas Jullien, Robert Viseur, Amel Charleux) ont mis en place un groupe de travail dont les premiers résultats ont été publiés en juin dernier dans un [livre blanc](https://inno3.fr/sites/default/files/2017-07/Livre%20blanc-evaluation-et-valorisation-des-actifs-immateriels-ouverts.pdf). Ce premier livrable a permis de faire un état des méthodologies existantes et de déterminer une ébauche à la fois des éléments importants à évaluer et d’une première méthodologie adaptée aux modèles de l’Open Source et de l’Open Data. 

Suivant cette méthode de travail, le projet de recherche vise à réunir un groupe de travail composé d’un ensemble hétérogène d’acteurs provenant du public comme du privé, mêlant diverses compétences juridiques et techniques mais également en matière de management de l’innovation. 