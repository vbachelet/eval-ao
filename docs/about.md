# À propos de nous

**Inno³** est un cabinet de conseil en innovation ouverte, indépendant et spécialiste de l'Open Data et de l'Open Source. De l'audit de code à l'évaluation des politiques de valorisation, les compétences du cabinet compétences s'étendent de la gestion de la propriété intellectuelle à la transformation d'organisation et à la mise en place de dynamiques d'écosystème.

**Nicolas Jullien** est professeur à IMT Atlantique, chercheur au LEGO. Ses disciplines de prédilections sont l'économie industrielle, le management de l’innovation, les business models de l'innovation ouverte. 

**Amel Charleux** est docteure en droit, chercheure à l'Université de Montpellier spécialisée notamment dans la coopétition et les business modèles de l'innovation ouverte.

**Robert Viseur** est chargé de cours d'informatique et de management de l'innovation et chef du service TIC de l'Université de Mons. Il est notamment spécialisé dans les domaines de l'open source, de l'innovation /et des industries culturelles et créatives. 

