# Contributors

## Comment contribuer

Ce projet constitue également un actif immatériel ouvert, destiné à s'enrichir des contributions de chacuns, aussi nous espérons vivement bénéficier de vos retours et/ou contributions, quel que soit votre domaine d'expertise. 
Vous pouvez directement contribuer sur ce dépôt en ouvrant une issue ou soumettant une merge request. Si cela vous convient plus, vous pouvez également nous écrire à : 

De part sa nature d'actif immatériel ouvert, toutes les contributions faites à ce projet sont placée sous licence [à déterminer]. 

## L'équipe actuelle

* Amel Charleux (ACH) - Université de Montpellier
* Nicolas Jullien (NJU) - IMT Atlantique
* Robert Viseur (RVI) - Université de Mons
* Benjamin Jean (BJE) - Inno³
* Laure Kassem (LKA) - Inno³
* Vincent Bachelet (VBA) - Inno³